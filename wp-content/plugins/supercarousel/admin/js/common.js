var SupercarouselCommon = new function () {
    var $$ = this;

    $$.get_super_thumb = function (img) {
        if (img['thumbnail']) {
            return img['thumbnail']['url'];
        } else if (img['medium']) {
            return img['medium']['url'];
        } else {
            return img['full']['url'];
        }
    }

    $$.super_add_google_font = function (FontName) {
        if (FontName == '') {
            return;
        }
        jQuery("head").append("<link href='https://fonts.googleapis.com/css?family=" + FontName + ":100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>");
    }

    $$.super_request_ajax = function (ajax_url, ajax_data, ajax_callback) {
        jQuery.post(ajax_url, ajax_data, function (response) {
            if (typeof ajax_callback == 'string') {
                if (ajax_callback != '') {
                    eval(ajax_callback + '(response)');
                }
            } else if (typeof ajax_callback == 'function') {
                ajax_callback(response);
            }
        });
    }

    $$.supercarousel_save = function (frm) {
        if (jQuery(frm)[0].checkValidity()) {
            if (typeof tinyMCE != 'undefined') {
                tinyMCE.triggerSave();
            }
            var data = jQuery(frm).serialize();
            jQuery('.su_status').html(SUPERTRANS.saving).show();
            $$.super_request_ajax(ajaxurl, data, $$.supercarousel_save_callback);
        }
        return false;
    }

    $$.delete_supercarousel = function (id) {
        var c = confirm(SUPERTRANS.delete_confirm);
        if (!c) {
            return;
        }
        var data = {};
        data.action = 'supercarouseladmin';
        data.superaction = 'delete_supercarousel';
        data.id = id;
        $$.super_request_ajax(ajaxurl, data, $$.supercarousel_save_callback);
    }

    $$.super_status_hide = function () {
        setTimeout(function () {
            jQuery('.su_status').fadeOut();
        }, 1000);
    }

    $$.supercarousel_save_callback = function (msg) {
        if (msg.msg == 'added') {
            window.location.href = 'admin.php?page=' + jQuery('#superpage').val() + '&act=edit&id=' + msg.post_id;
        } else if (msg.msg == 'deleted') {
            window.location.href = 'admin.php?page=' + jQuery('#superpage').val();
        } else {
            jQuery('.su_status').html(SUPERTRANS.saved);
            $$.super_status_hide();
        }
    }

    $$.sanitize_title_with_dashes = function ($title) {
        $title = strip_tags($title);
        // Preserve escaped octets.
        $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
        // Remove percent signs that are not part of an octet.
        $title = str_replace('%', '', $title);
        // Restore octets.
        $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

        if (seems_utf8($title)) {
            if (function_exists('mb_strtolower')) {
                $title = mb_strtolower($title, 'UTF-8');
            }
            $title = utf8_uri_encode($title, 200);
        }

        $title = strtolower($title);
        $title = preg_replace('/&.+?;/', '', $title); // kill entities
        $title = str_replace('.', '-', $title);
        $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
        $title = preg_replace('/\s+/', '-', $title);
        $title = preg_replace('|-+|', '-', $title);
        $title = trim($title, '-');

        return $title;
    }

    $$.splitStringToParts = function (str, delim, n) {
        var arr = str.split(delim);
        var arr1 = arr.slice(0, n);
        var str1 = arr.slice(n, arr.length).join(delim);
        arr1.push(str1)
        return arr1;
    }

    $$.fixSaveButtonBox = function () {
        if(jQuery('.super_save_box_position').position().top > jQuery(window).height()) {
            return;
        }
        if (jQuery(window).scrollTop() > (jQuery('.super_save_box_position').position().top + 50)) {
            jQuery('.supersettings_save_box').addClass('superfixed');
            jQuery('.supersettings_save_box').width(jQuery('.super_save_box_position').width() - 2);
            jQuery('.super_save_box_position').height(jQuery('.supersettings_save_box').height() + 10);
        } else {
            jQuery('.supersettings_save_box').removeClass('superfixed');
            jQuery('.supersettings_save_box').css({width: 'auto'});
            jQuery('.super_save_box_position').css({height: 'auto'});
        }
    }

    jQuery(document).ready(function () {
        jQuery('.susettingtoggle').parent().click(function (e) {
            e.preventDefault();
            var $this = jQuery(this);
            if ($this.find('.susettingtoggle').hasClass('dashicons-plus')) {
                $this.find('.susettingtoggle').removeClass('dashicons-plus').addClass('dashicons-minus');
                $this.parent().find('.susettingboxcontent').slideDown();
            } else {
                $this.find('.susettingtoggle').removeClass('dashicons-minus').addClass('dashicons-plus');
                $this.parent().find('.susettingboxcontent').slideUp();
            }
        });

        if (jQuery('.noentersubmit').length) {
            jQuery('.noentersubmit').keypress(function (event) {
                if (event.which == 13) {
                    if (jQuery(this).data('entercallback')) {
                        if (jQuery(this).data('entercallback') != '') {
                            eval(jQuery(this).data('entercallback'));
                        }
                    }
                    return false;
                }
            });
        }

        if (jQuery('.sucolor').length) {
            jQuery('.sucolor').spectrum({
                flat: false,
                showInput: true,
                showAlpha: true,
                preferredFormat: "rgb",
                showButtons: false
            });
        }

        if (jQuery('.supersettings_save_box').length) {
            jQuery(window).scroll(function () {
                $$.fixSaveButtonBox();
            });
            jQuery(window).resize(function () {
                $$.fixSaveButtonBox();
            });
        }
    });
};

jQuery.fn.selectText = function () {
    this.find('input').each(function () {
        if ($(this).prev().length == 0 || !$(this).prev().hasClass('p_copy')) {
            $('<p class="p_copy" style="position: absolute; z-index: -1;"></p>').insertBefore($(this));
        }
        $(this).prev().html($(this).val());
    });
    var doc = document;
    var element = this[0];
    console.log(this, element);
    if (doc.body.createTextRange) {
        var range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
};