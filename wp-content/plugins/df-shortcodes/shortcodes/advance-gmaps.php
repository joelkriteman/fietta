<?php

function advance_gmaps( $atts, $content = null) {

	$output = $output_img = $output_img_el = $parallax_class = $maps = '';

	extract( shortcode_atts( array(
		'height' 			=> '400',
		'latitude' 			=> '',
		'longitude' 		=> '',
		'address' 			=> '',
		'latitude_2' 		=> '',
		'longitude_2' 		=> '',
		'address_2' 		=> '',
		'zoom' 				=> '14',
		'pan_control' 		=> 'true',
		'draggable' 		=> 'true',
		'zoom_control' 		=> 'true',
		'map_type_control' 	=> 'true',
		'scale_control' 	=> 'true',
		'img' 				=> '',
		'modify_coloring' 	=> 'false',
		'hue' 				=> '#ccc',
		'saturation' 		=> '-50',
		'lightness' 		=> '0',
		'el_class' 			=> ''
	), $atts ) );

	$img 			 = explode( ',', $img );
	$i 				 = -1;
	$id 			 = mt_rand(99,9999);

	foreach ( $img as $attach_id ) { $i++;
        $image_src   = wp_get_attachment_image_src( $attach_id, 'thumbnail' );
        $output_img .= $image_src[0];
    }

	if ( $longitude == '' && $latitude == '') { return null; }

	if ( $zoom < 1 ) { $zoom = 1; }

	$maps .= '<div id="google-map-'.$id.'" class="advanced-gmaps '.$el_class.'" style="height:'.$height.'px;width:100%;" data-zoom="'.$zoom.'" data-pin-icon="'.$output_img.'" data-latitude="'.$latitude.'" data-longitude="'.$longitude.'" data-address="'.$address.'" data-latitude2="'.$latitude_2.'" data-longitude2="'.$longitude_2.'" data-address2="'.$address_2.'" data-pan-control="'.$pan_control.'" data-zoom-control="'.$zoom_control.'" data-map-type-control="'.$map_type_control.'" data-scale-control="'.$scale_control.'" data-draggable="'.$draggable.'" data-modify-coloring="'.$modify_coloring.'" data-saturation="'.$saturation.'" data-lightness="'.$lightness.'" data-hue="'.$hue.'"></div>';
	$maps .= '<script src="http'.( is_ssl() ? 's' : '' ).'://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>';

	return $maps;

}
add_shortcode( 'df_advanced_gmaps', 'advance_gmaps' );