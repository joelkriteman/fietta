<?php
if (!defined('ABSPATH')) die('-1');

function df_modal_box($atts, $content = null) {
	$style = $modal_class = $modal_data_class = $html = $border_style = $header_style = $modal_title = $modal_contain = $content_style = $box_icon = '';
	extract(shortcode_atts(array(
		'icon_type'				=> 'fa_icon',
		'type'					=> '',
		'rotate'				=> '',
		'size'					=> '',
		'flip'					=> '',
		'icon_img' 				=> '',
		'modal_on' 				=> '',
		// button normal with icon trigger
		'btn_size' 				=> '',
		'btn_bg_color' 			=> '',
		'btn_txt_color' 		=> '',
		'btn_text' 				=> '',
		// image button trigger
		'btn_img' 				=> '',
		// text button trigger
		'read_text' 			=> '',
		'txt_color' 			=> '',
		// onload trigger
		'onload_delay'			=> '',
		'modal_on_align' 		=> '',
		// modal box
		'modal_size' 			=> '',
		'modal_style'			=> 'overlay-fade',
		'content_bg_color' 		=> '',
		'content_text_color' 	=> '',
		'header_bg_color' 		=> '',
		'header_text_color' 	=> '',
		'modal_title' 			=> '',
		'modal_contain' 		=> '',
		'el_class' 				=> ''
	), $atts ));

	ob_start();
	$image_src    = wp_get_attachment_image_src( $btn_img, 'full' );
	$icon_img_src = wp_get_attachment_image_src( $icon_img, 'full' );
	$output_img   = $image_src[0];
	$icon_img     = $icon_img_src[0];
	$logo 		  = '<i class="fa '.$type.'"></i>' ;
	$uniq 		  = mt_rand(99,9999);

	// Create style for content background color
	if($content_bg_color !== '') { $content_style .= 'background:'.$content_bg_color.';'; }
	// Create style for content text color
	if($content_text_color !== '') { $content_style .= 'color:'.$content_text_color.';'; }
	// Create style for header background color
	if($header_bg_color !== '') { $header_style = 'background:'.$header_bg_color.';'; }
	// Create style for header text color
	if($header_text_color !== '') { $header_text_color = 'color:'.$header_text_color.';'; }

	// modal style class
	if($modal_style != 'overlay-show-cornershape' && $modal_style != 'overlay-show-genie' && $modal_style != 'overlay-show-boxes'){
		$modal_class = 'overlay-show';
		$modal_data_class = 'data-overlay-class="'.$modal_style.'"';
	} else {
		$modal_class = $modal_style;
		$modal_data_class = '';
	}
	// icon style
	if($icon_type == 'custom') {
		$box_icon = '<div class="modal-icon"><img src="'.$icon_img.'" class="df-modal-inside-img"></div>';
	} elseif($icon_type == 'fa_icon'){
		$box_icon = '<div class="modal-icon" style="'.$header_text_color.'">'.$logo.'</div>';
	}
	// trigger
	switch ($modal_on) {
		case 'button':
			if( $btn_bg_color !== '' ){
				$style .= 'background:'.$btn_bg_color.';';
				$style .= 'border-color:'.$btn_bg_color.';'; }

			if( $btn_txt_color !== '' ){ $style .= 'color:'.$btn_txt_color.';'; }

			$html .= '<button style="'.$style.'" data-class-id="content-'.$uniq.'" class="df-modal-box-sc btn btn-primary btn-'.$btn_size.' '.$modal_class.' df-align-'.$modal_on_align.'" '.$modal_data_class.'>'.$btn_text.'</button>';
			break;
		case 'image':
			if($output_img !==''){
				$html .= '<img src="'.$output_img.'" data-class-id="content-'.$uniq.'" class="df-modal-box-sc  df-modal-img '.$modal_class.' df-align-'.$modal_on_align.'" '.$modal_data_class.'/>';
			}
			break;
		case 'onload':
			$html .= '<div data-class-id="content-'.$uniq.'" class="df-modal-box-sc  df-onload '.$modal_class.' " '.$modal_data_class.' data-onload-delay="'.$onload_delay.'"></div>';
			break;
		case 'text':
			if($txt_color !== ''){
				$style .= 'color:'.$txt_color.';'; $style .= 'cursor:pointer;';
			}
			$html .= '<span style="'.$style.'" data-class-id="content-'.$uniq.'" class="df-modal-box-sc '.$modal_class.' df-align-'.$modal_on_align.'" '.$modal_data_class.'>'.$read_text.'</span>';
			break;
		default:
			if( $btn_bg_color !== '' ){
				$style .= 'background:'.$btn_bg_color.';';
				$style .= 'border-color:'.$btn_bg_color.';'; }

			if( $btn_txt_color !== '' ){ $style .= 'color:'.$btn_txt_color.';'; }

			$html .= '<button style="'.$style.'" data-class-id="content-'.$uniq.'" class="df-modal-box-sc btn btn-primary btn-'.$btn_size.' '.$modal_class.' df-align-'.$modal_on_align.'" '.$modal_data_class.'>'.$btn_text.'</button>';
			break;
	}

	// modal box
	if($modal_style == 'overlay-show-cornershape') {
		$html .= '<div class="df-overlay overlay-cornershape content-'.$uniq.' '.$el_class.'" style="display:none" data-class="content-'.$uniq.'" data-path-to="m 0,0 1439.999975,0 0,805.99999 -1439.999975,0 z">';
    	$html .= '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1440 806" preserveAspectRatio="none">
        			  <path class="overlay-path" d="m 0,0 1439.999975,0 0,805.99999 0,-805.99999 z"/>
    			  </svg>';
	} else if ($modal_style == 'overlay-show-genie') {
		$html .= '<div class="df-overlay overlay-genie content-'.$uniq.' '.$el_class.'" style="display:none" data-class="content-'.$uniq.'" data-steps="m 701.56545,809.01175 35.16718,0 0,19.68384 -35.16718,0 z;m 698.9986,728.03569 41.23353,0 -3.41953,77.8735 -34.98557,0 z;m 687.08153,513.78234 53.1506,0 C 738.0505,683.9161 737.86917,503.34193 737.27015,806 l -35.90067,0 c -7.82727,-276.34892 -2.06916,-72.79261 -14.28795,-292.21766 z;m 403.87105,257.94772 566.31246,2.93091 C 923.38284,513.78233 738.73561,372.23931 737.27015,806 l -35.90067,0 C 701.32034,404.49318 455.17312,480.07689 403.87105,257.94772 z;M 51.871052,165.94772 1362.1835,168.87863 C 1171.3828,653.78233 738.73561,372.23931 737.27015,806 l -35.90067,0 C 701.32034,404.49318 31.173122,513.78234 51.871052,165.94772 z;m 52,26 1364,4 c -12.8007,666.9037 -273.2644,483.78234 -322.7299,776 l -633.90062,0 C 359.32034,432.49318 -6.6979288,733.83462 52,26 z;m 0,0 1439.999975,0 0,805.99999 -1439.999975,0 z">';
		$html .= '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1440 806" preserveAspectRatio="none">
					  <path class="overlay-path" d="m 701.56545,809.01175 35.16718,0 0,19.68384 -35.16718,0 z"/>
				  </svg>';
	} else if($modal_style == 'overlay-show-boxes') {
		$html .= '<div class="df-overlay overlay-boxes content-'.$uniq.' '.$el_class.'" style="display:none" data-class="content-'.$uniq.'">';
		$html .= '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="101%" viewBox="0 0 1440 806" preserveAspectRatio="none">
					  <path d="m0.005959,200.364029l207.551124,0l0,204.342453l-207.551124,0l0,-204.342453z"/>
					  <path d="m0.005959,400.45401l207.551124,0l0,204.342499l-207.551124,0l0,-204.342499z"/>
					  <path d="m0.005959,600.544067l207.551124,0l0,204.342468l-207.551124,0l0,-204.342468z"/>
					  <path d="m205.752151,-0.36l207.551163,0l0,204.342437l-207.551163,0l0,-204.342437z"/>
					  <path d="m204.744629,200.364029l207.551147,0l0,204.342453l-207.551147,0l0,-204.342453z"/>
					  <path d="m204.744629,400.45401l207.551147,0l0,204.342499l-207.551147,0l0,-204.342499z"/>
					  <path d="m204.744629,600.544067l207.551147,0l0,204.342468l-207.551147,0l0,-204.342468z"/>
					  <path d="m410.416046,-0.36l207.551117,0l0,204.342437l-207.551117,0l0,-204.342437z"/>
					  <path d="m410.416046,200.364029l207.551117,0l0,204.342453l-207.551117,0l0,-204.342453z"/>
					  <path d="m410.416046,400.45401l207.551117,0l0,204.342499l-207.551117,0l0,-204.342499z"/>
					  <path d="m410.416046,600.544067l207.551117,0l0,204.342468l-207.551117,0l0,-204.342468z"/>
					  <path d="m616.087402,-0.36l207.551086,0l0,204.342437l-207.551086,0l0,-204.342437z"/>
					  <path d="m616.087402,200.364029l207.551086,0l0,204.342453l-207.551086,0l0,-204.342453z"/>
					  <path d="m616.087402,400.45401l207.551086,0l0,204.342499l-207.551086,0l0,-204.342499z"/>
					  <path d="m616.087402,600.544067l207.551086,0l0,204.342468l-207.551086,0l0,-204.342468z"/>
					  <path d="m821.748718,-0.36l207.550964,0l0,204.342437l-207.550964,0l0,-204.342437z"/>
					  <path d="m821.748718,200.364029l207.550964,0l0,204.342453l-207.550964,0l0,-204.342453z"/>
					  <path d="m821.748718,400.45401l207.550964,0l0,204.342499l-207.550964,0l0,-204.342499z"/>
					  <path d="m821.748718,600.544067l207.550964,0l0,204.342468l-207.550964,0l0,-204.342468z"/>
					  <path d="m1027.203979,-0.36l207.550903,0l0,204.342437l-207.550903,0l0,-204.342437z"/>
					  <path d="m1027.203979,200.364029l207.550903,0l0,204.342453l-207.550903,0l0,-204.342453z"/>
					  <path d="m1027.203979,400.45401l207.550903,0l0,204.342499l-207.550903,0l0,-204.342499z"/>
					  <path d="m1027.203979,600.544067l207.550903,0l0,204.342468l-207.550903,0l0,-204.342468z"/>
					  <path d="m1232.659302,-0.36l207.551147,0l0,204.342437l-207.551147,0l0,-204.342437z"/>
					  <path d="m1232.659302,200.364029l207.551147,0l0,204.342453l-207.551147,0l0,-204.342453z"/>
					  <path d="m1232.659302,400.45401l207.551147,0l0,204.342499l-207.551147,0l0,-204.342499z"/>
					  <path d="m1232.659302,600.544067l207.551147,0l0,204.342468l-207.551147,0l0,-204.342468z"/>
					  <path d="m-0.791443,-0.360001l207.551163,0l0,204.342438l-207.551163,0l0,-204.342438z"/>
				  </svg>';
	} else {
		$html .= '<div class="df-overlay content-'.$uniq.' '.$el_class.'" data-class="content-'.$uniq.'" id="button-click-overlay" style="display:none;">';
	}
	$html .= '<div class="df_modal df-fade df-'.$modal_size.'">';
	$html .= '<div class="df_modal-content">';
	if($modal_title !== ''){
		$html .= '<div class="df_modal-header" style="'.$header_style.'">';
		$html .= $box_icon.'<h3 class="df_modal-title" style="'.$header_text_color.'">'.$modal_title.'</h3>';
		$html .= '</div>';
	}
	$html .= '<div class="df_modal-body '.$modal_contain.'" style="'.$content_style.'">';
	$html .= do_shortcode($content);
	$html .= '</div>';
	$html .= '</div>';
	$html .= '</div>';
	$html .= '<div class="df-overlay-close">Close</div>';
	$html .= '</div>';
	$html .= ob_get_clean();
	return $html;
}
add_shortcode( 'df_modal', 'df_modal_box' );