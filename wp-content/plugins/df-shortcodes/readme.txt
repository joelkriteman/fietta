####################### Changelog ######################
Progress Version 1.3.3 Oct 9 2015
* Update compability to visual composer 4.7.4

Progress Version 1.3.2 July 29th 2015
* Fixed: advance gmaps
* Fixed: modal box

Version 1.3.1 July 6th 2015
* Compability issue visual composer 4.6

Version 1.3.1 June 28th 2015
* service fix bugs icon

Version 1.3 May 28th 2015
* add image icon in service shortcode
* add two layouts in service shortcode (right with icon and right icon)
* Reset Single Image Line Height
* Fixed all carousel shortcode
* Fixed advance google map ssl and jquery
* Fixed anchor

Version 1.2 May 13th 2015
* Fixed all carousel shortcode

Version 1.1 April 30th 2015
* add compability with visual composer 4.5

Version 1.0 April 29th 2015
* Initial Release