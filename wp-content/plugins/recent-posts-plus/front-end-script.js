(function(){

	/**
	 * rppw : recent post plus widget
	 */
	var rppw_container = jQuery('[id*=recent-posts-plus]');


	// do not process further if no rppw exist on page
	if( ! rppw_container.length ){
		return;
	}

	// rearrage posts in list
	rppw_container.each( function( i, k ){
		// unique key for local storage
		var id_name = jQuery( this ).attr( 'id' );

		if( ! window.localStorage ){
			return;
		}

		// post ids
		var post_ids = JSON.parse( localStorage.getItem( id_name ) );

		var posts_html = '';

		for ( id in post_ids ){
			var post_container = jQuery( '[data-id="'+ post_ids[id] +'"]', jQuery( this ) ).parents('li');
			posts_html += '<li>' + post_container.html() + '</li>' ;
			post_container.remove();
		}

		jQuery( 'ul', this ).append( posts_html );

		// hide ajax loader image
		jQuery( '.ajax-loader', this ).hide();

		// show modified posts html
		jQuery( 'ul', this ).show();
	});

	// add event to each anchor link with class 'post-link'
	rppw_container.each( function( i, k ){

		// unique key for local storage
		var id_name = jQuery( this ).attr( 'id' );

		jQuery( this ).on( 'click', '.post-link', function( e ){
			e.preventDefault();
			
			if( window.localStorage ){

				var post_ids = JSON.parse( localStorage.getItem( id_name ) );

				
				// check item already exist in local storage, if not then intialize empty array
				if( ! post_ids ){
					post_ids = [];
				}

				var post_id = jQuery(this).data( 'id' );

				// check if value already exist in array
				if( jQuery.inArray( post_id, post_ids ) > -1 ){
					return false;
				}

				// push current post id to array
				post_ids.push( post_id );

				// store data in local storage
				localStorage.setItem( id_name, JSON.stringify( post_ids ) );
			}

			return false;
		})
	});

})();