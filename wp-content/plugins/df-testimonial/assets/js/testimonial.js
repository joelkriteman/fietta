jQuery(document).ready(function($) {
	if ( $( 'div' ).hasClass('slider-testimonial') ) {
		$('.slider-testimonial').owlCarousel({
			rtl: ( $('body').hasClass('rtl') ) ? true : false,
			margin: 30,
			items: 1,
			autoHeight: true,
		});
		$('.slider-testimonial').addClass('owl-carousel owl-theme');
	}
});