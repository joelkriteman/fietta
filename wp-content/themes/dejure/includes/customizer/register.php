<?php 

if ( ! function_exists( 'df_customizer_get_panel' ) ) :
/**
 * Get Panel Customizer
 * name 0 | title 1 | priority 2 | description 3
 * @return array
 */
function df_customizer_get_panel(){
	$panels = array(
		'header_panel'  => array( 'title' => 'Header', 'priority' => 10, 'description' => _x( 'Header is first impression of your website here you can customize the whole part of header area.', 'backend customizer', 'backend_dahztheme' ) ),
		'content_panel' => array( 'title' => 'Content', 'priority' => 20, 'description' => _x( 'Customize your typography and the outer & content area which includes background image, color, and sidebar layout.', 'backend customizer', 'backend_dahztheme' ) ),
		'footer_panel'  => array( 'title' => 'Footer', 'priority' => 30, 'description' => _x( 'Footer refers to the bottom section that contains information copyright notices, links to privacy policy, credits and widgetized area with multiple columns that you can use to add widgets.', 'backend customizer', 'backend_dahztheme' ) ),
		'blog_panel'    => array( 'title' => 'Blog', 'priority' => 50, 'description' => _x( 'Customize your blog appearance, including layout, pagination style, featured slider, archive and sharing options.', 'backend customizer', 'backend_dahztheme' ) ),
		'misc_panel'    => array( 'title' => 'Misc', 'priority' => 60, 'description' => _x( 'Misc is additional configuration which includes site icon, google analytics, social media URLs, 404 page, page loader and font subset.', 'backend customizer', 'backend_dahztheme' ) )
		);

	return apply_filters( 'create_customizer_panels', $panels );
}
endif;

/**
 * Get Section Customizer
 * @return array
 */
function df_customizer_get_section(){
	$section = array(
		/* Section : Screen Layout Mode */
		'general_section' => array('title' => _x('General', 'backend customizer','backend_dahztheme'), 'priority' => 0),
		/* Section : Button Mode */
		'button_section' => array('title' => _x('Button', 'backend customizer','backend_dahztheme'), 'priority' => 40),

		// Panel: Header
		/* Section : Topbar */
		'topbar_section' => array('title' => _x('Topbar', 'backend customizer', 'backend_dahztheme'), 'priority' => 5, 'panel' => 'df_customizer_header_panel'),
		/* Section : Logo Setting */
		'logo_section' => array('title' => _x('Logo', 'backend customizer', 'backend_dahztheme'), 'priority' => 10, 'panel' => 'df_customizer_header_panel'),
		/* Section : Navbar */
		'navbar_section' => array('title' => _x('Navbar', 'backend customizer', 'backend_dahztheme'), 'priority' => 15, 'panel' => 'df_customizer_header_panel'),
		/* Section : Navbar Transparency */
		'navbar_transparency_section' => array('title' => _x('Transparency', 'backend customizer', 'backend_dahztheme'), 'priority' => 20, 'panel' => 'df_customizer_header_panel'),
		/* Section : Sticky */
		'navbar_sticky_section' => array('title' => _x('Sticky', 'backend customizer', 'backend_dahztheme'), 'priority' => 25, 'panel' => 'df_customizer_header_panel'),
		/* Section : Miscellaneous */
		'navbar_miscellaneous_section' => array('title' => _x('Miscellaneous', 'backend customizer', 'backend_dahztheme'), 'priority' => 30, 'panel' => 'df_customizer_header_panel'),
		/* Section : Page Title */
		'pagetitle_section' => array('title' => _x('Page Title', 'backend customizer', 'backend_dahztheme'), 'priority' => 35, 'panel' => 'df_customizer_header_panel'),

		// Panel: Content
	    /* Section : Outer Area */
	    'outer_area_section' => array('title' => _x('Outer Area', 'backend customizer','backend_dahztheme'), 'priority'	=> 0, 'panel' => 'df_customizer_content_panel'),
	    /* Section : Content Area */
	    'content_area_section' => array('title'	=> _x('Content Area', 'backend customizer','backend_dahztheme'), 'priority'	=> 10, 'panel' => 'df_customizer_content_panel'),
	    /* Section : Typography */
	    'typo_section' => array('title'	=> _x('Typography', 'backend customizer','backend_dahztheme'),'priority' => 20, 'panel'	=> 'df_customizer_content_panel'),

		// Panel: Footer
		/* Section : Widget Footer */
		'primary_section' => array('title' => _x('Primary Footer', 'backend customizer', 'backend_dahztheme'), 'priority' => 5, 'panel'	=> 'df_customizer_footer_panel'),
		/* Section : Copyright Footer */
		'copyright_section' => array('title' => _x('Copyright Footer', 'backend customizer', 'backend_dahztheme'), 'priority' => 10, 'panel' => 'df_customizer_footer_panel'),

		// Panel: Blog
	    /* Section : Featured Slider */
	    'featslider_section' => array('title' => _x('Featured Slider', 'backend customizer', 'backend_dahztheme'), 'priority' => 0,	'panel'	=> 'df_customizer_blog_panel'),
	    /* Section : Layout */
	    'layout_section' => array('title' => _x('Layout', 'backend customizer', 'backend_dahztheme'), 'priority' => 10, 'panel'	=> 'df_customizer_blog_panel'),
	    /* Section : Single Blog */
	    'singleblog_section' => array('title' => _x('Single Blog', 'backend customizer', 'backend_dahztheme'), 'priority'=> 20, 'panel' => 'df_customizer_blog_panel'),
	    /* Section : Archive */
	    'archive_section' => array('title' => _x('Archive', 'backend customizer', 'backend_dahztheme'), 'priority' => 30, 'panel' => 'df_customizer_blog_panel'),
	    /* Section : Share */
	    'share_section' => array( 'title' => _x('Share', 'backend customizer', 'backend_dahztheme'), 'priority'	=> 40, 'panel' => 'df_customizer_blog_panel'),

	    // Panel: Misc
	    /* Favicon */
	    'site_icon_section' => array('title' => _x('Site Icon', 'backend customizer', 'backend_dahztheme'), 'priority' => 5, 'panel' => 'df_customizer_misc_panel'),
	    /* Google Analytics */
	    'google_analytics_section' => array('title'	=> _x('Google Analytics', 'backend customizer', 'backend_dahztheme'), 'priority' => 10, 'panel'	=> 'df_customizer_misc_panel'),
	    /* Social Connects */
	    'social_connect_section' => array('title' => _x('Social Connect', 'backend customizer', 'backend_dahztheme'), 'priority' => 15, 'panel' => 'df_customizer_misc_panel'),
	    /* 404 */
	    '404_section' => array('title' => _x('404', 'backend customizer', 'backend_dahztheme'),	'priority'		=> 20, 'panel' => 'df_customizer_misc_panel'),
	    /* Page Loader */
	    'page_loader_section' => array('title' => _x('Page Loader', 'backend customizer', 'backend_dahztheme'), 'priority' => 25, 'panel' => 'df_customizer_misc_panel'),
	    /* Font Subsets */
	    'font_subset_section' => array('title' => _x('Font Subset', 'backend customizer', 'backend_dahztheme'), 'priority' => 30, 'panel' => 'df_customizer_misc_panel'),

		// Custom CSS
		'custom_style_section' => array('title' => _x('Custom CSS', 'backend customizer', 'dahztheme'), 'priority' => 1000) );

		return apply_filters( 'create_customizer_section', $section );
}

/**
 * Register Theme Customizer
 */
function df_register_panel_customizer( $wp_customize ){
	if ( ! isset( $wp_customize ) ) {
		return;
	}

		$prefix  = 'df_customizer_';
		$panels  = df_customizer_get_panel();
        $section = df_customizer_get_section();

		/* Section : Screen Layout Mode */
		$wp_customize->add_section( $prefix . 'general_section', array('title' => _x('General', 'backend customizer','backend_dahztheme'), 'priority' => 0 ) );
		/* Section : Button Mode */
		$wp_customize->add_section( $prefix . 'button_section', array('title' => _x('Button', 'backend customizer','backend_dahztheme'), 'priority' => 40 ) );
		/* Section : Custom CSS */
		$wp_customize->add_section( $prefix . 'custom_style_section', array('title' => _x('Custom CSS', 'backend customizer', 'dahztheme'), 'priority' => 1000 ) );

		// Tier 1 : Add Panel
		foreach ( $panels as $name => $data ) {
			$wp_customize->add_panel( $prefix . $name, $data );
		}

		// Tier 2: Add Section
		foreach ( $section as $name => $data ) {
			$wp_customize->add_section( $prefix . $name, $data );
		}

		// Tier 1 : Add Panel
		// foreach ( $panels as $panel ) {

		// 	$wp_customize->add_panel( $prefix . $panel[0], array(
		// 		'title' => $panel[1],
		// 		'priority' => $panel[2],
		// 		'description' => $panel[3]
		// 	) );
		// 	// Tier 2 : Add Section
		// 	foreach ( $panel['sections'] as $section ) {
		// 		$wp_customize->add_section(	$prefix . $section[0],	array(
		// 				'title'			=> $section[1],
		// 				'priority'  => $section[2],
		// 				'panel'			=> $prefix . $panel[0]
		// 			)
		// 		);
		// 	}
		// }

}

function df_register_controls( $wp_customize ){

		df_register_panel_customizer( $wp_customize );

        $options_path = get_template_directory() . '/includes/customizer/option-settings';
        require( $options_path . '/variable-option.php' );
        require( $options_path . '/customizer-header-options.php' );
        require( $options_path . '/customizer-misc-options.php' );
        require( $options_path . '/customizer-content-options.php' );
        require( $options_path . '/customizer-footer-options.php' );
        require( $options_path . '/customizer-blog-options.php' );
        require( $options_path . '/customizer-button-options.php' );
        require( $options_path . '/customizer-general-options.php' );
        require( $options_path . '/customizer-customcss-options.php' );

		dahz_build_customizer( $wp_customize, $controls );

}

/* ----------------------------------------------------------------------------------- */
/* Theme Customizer JavaScript                                                         */
/* ----------------------------------------------------------------------------------- */
function df_enqueue_customizer_admin_scripts() {
		$suffix = dahz_get_min_suffix();
        wp_enqueue_script( 'df-customizer-admin', get_template_directory_uri() . '/includes/assets/js/admin/theme-customizer-control'.$suffix.'.js', array( 'jquery', 'customize-controls' ), NULL, true );
}

/* ----------------------------------------------------------------------------------- */
/* Theme Customizer Preview JavaScript                                                 */
/* ----------------------------------------------------------------------------------- */
function df_enqueue_customizer_admin_preview_scripts() {
		$suffix = dahz_get_min_suffix();
         wp_enqueue_script( 'df-customizer-preview', get_template_directory_uri() . '/includes/assets/js/admin/theme-customizer-preview'.$suffix.'.js', array( 'customize-preview' ), NULL, true );

         do_action( 'df_customizer_preview_localize' );
}