################ Support & Documentation ################
DOCUMENTATION  : http://support.daffyhazan.com/docs/dejure
SUPPORT FORUM  : http://support.daffyhazan.com/forums/our-template/dejure
KNOWLEDGE BASE : http://support.daffyhazan.com/section/dejure

####################### Changelog ######################
Version 1.5.3
* Compatibility : WordPress 4.5
* Update : Visual Composer 4.11.1
* Update : Revolution Slider 5.2
* Update : Splash Screen Detail

Version 1.5.2
* compatibility with Visual Composer 4.9.2

Version 1.5.1 
* visual composer compatibility issue (df shortcode)

Version 1.5 
* update tgmpa plugin
* visual composer compatibility issue (df shortcode)
* update admin home screen
* compatibility with wordpress 4.4

Version 1.4.0
* Updated: DahzFramework Core 2.2.0
* Updated: Revolution Slider 5.1
* Updated: Visual Composer 4.8
* Updated: DF Shortcodes

Version 1.3.0
* Updated: DahzFramework Core 2.0.0
* Added: meta-box-tab plugin to improve UX
* Updated: visual composer 4.7.4

Version 1.2.0 June 29th 2015
* Fixed: prettyPhoto XSS Vulnerabilty
* Changed: remove #content-wrap padding
* Added: margin-bottom title controller
* Added: use grid for main and sidebar layout.
* Changed: (title-controller.php) use get_the_archive_title for archive title
* Changed: meta-box folder removed from dahz folder to includes/admin/plugins/metabox.zip, and activate with TGM. to prevent conflict, In this case we can’t be sure which version of the plugin is used and that can makes website broken
* Fixed: enable image on share button
* Fixed: like button in infinite scroll
* Fixed: page search sidebar
* Fixed: comment template translation
* Fixed: Breadcrumbs
* Fixed: Anchor
* Updated: twitter-widget.js
* Updated: update sidebar generator 1.2.1
* Updated: Visual Composer 4.5.3
* Updated: Df shortcode
* Improved: Array panel and section customizer

Version 1.1.2 May 13th 2015
* fixed: undefined index layout content 404 not returned
* fixed: grid blog 3 col column
* added: fired animation frame on main.js
* removed: (custom-attribute.php) apply filters main content class & sidebar content class
* changed: logo transparent logic, switch statement replace by if statement

Version 1.1.1 May 4th 2015
* added: plugin envato toolkit
* changed: (primary.php, off-canvas.php ) df_get_layout_content_class() replaced by DF_part_layout::df_get_layout_content_class()
* removed: global $post on title-controller.php
* updated: Visual Composer 4.5

Version 1.1.0 April 30th 2015
* added: dejure child theme
* added: part-layout.php on folder modules
* improved: (part-header.php, part-footer.php ) use object oriented
* improved: update plugin Dahz DF Shortcode v.1.1
* fixed: footer widget column is none appear
* removed: description panel customizer
* removed: globe icon on misc header section
* removed: protocol google font

Version 1.0.0 April 29th 2015
* Initial Release
