<?php
/*
Template Name: Brexit Insights
*/
?>

<?php get_header(); ?>
	
	<div id="content" class="pil">
		
		<div id="inner-content" class="row brexit">
		     
		    <main id="main" class="large-8 large-offset-1 medium-8 columns end" role="main">

				<div>
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
			    	<?php get_template_part( 'parts/loop', 'brexit' ); ?>
			    
			    <?php endwhile; endif; ?>
				
				</div>	
				
				<hr />
				
				<div class="brexitsummary">
				
<?php if ( get_query_var( 'paged' ) ) {
	$paged = get_query_var( 'paged' );
} else if ( get_query_var( 'page' ) ) {
	// This will occur if on front page.
	$paged = get_query_var( 'page' );
} else {
	$paged = 1;
}
$my_query = new WP_Query( array(
	'post_type'           => 'brexit',
	'posts_per_page'      => 10,
	'paged'               => $paged,
) );
while ( $my_query->have_posts() ) : $my_query->the_post(); ?>

<div class="brexititem">
	<a href="<?php the_permalink(); ?>" class="cptui-link thumbnail-container">
							<?php
								/**
								 * Filters the thumbnail size to use for template file.
								 *
								 * @since 1.5.3
								 *
								 * @param string $value      Thumbnail size to use. Default 'post-thumbnail'.
								 * @param array  $attributes Shortcode attributes.
								 * @param int    $value      Current post ID.
								 */
								the_post_thumbnail( apply_filters( 'template_grid_thumbnail_size', 'post-thumbnail', $attributes, get_the_ID() ) );
							?>
 						</a>
 						<div class="brexitsummarytext">
 								<p><span class="brextitle"><?php the_title(); ?></span>&nbsp;&nbsp; <?php the_field('post_summary'); ?>
 									<?php if( get_field('final_link') ): ?> 
 										<a href="<?php the_field('final_link'); ?>" target="_blank">here</a>
 									<?php else: // field_name returned false ?>
 										<a href="<?php the_permalink(); ?>">Read&nbsp;more&nbsp;&#187;</a></p>
 									<?php endif; // end of if field_name logic ?>
 						</div>
 							<br />
 							<div class="clear"></div>
</div> 							

	<?php

endwhile;
printf( '<div class="prevnext">%s</div>', get_previous_posts_link( 'Newer Posts', $my_query->max_num_pages ) );
printf( '<div class="prevnext">%s</div>', get_next_posts_link( 'Older Posts', $my_query->max_num_pages ) ); ?>

				
				<br />
				
				</div>
				
				
				<hr />
				  					
			</main> <!-- end #main -->
			
			 <?php get_sidebar('brexit'); ?>
			 

		</div> <!-- end #inner-content -->
		
		<div class="keylinks">
				<h2>Key Links</h2>
				<p><?php the_field('key_links'); ?></p>
		</div>

	</div> <!-- end #content -->


<?php get_footer(); ?>

