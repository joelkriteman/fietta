<article <?php dahz_attr( 'post' ); ?>>

	<?php do_action( 'df_image_normal_post' ); ?>

	<div class="df-post-content">

		<?php if ( is_single( get_the_ID() ) ) : // If viewing a single post. ?>

			<div <?php dahz_attr( 'entry-content' ); ?>>

				<?php the_title( '<h3 class="blog-title">', '</h3>' ); ?>
				
				<?php the_field('single_post'); ?>



			</div><!-- .entry-content -->


		<?php else : // If not viewing a single post. ?>

			<header class="entry-header">

				<?php the_title( '<h3 class="blog-title">', '</h3>' ); ?>


			</header><!-- .entry-header -->

			<div <?php dahz_attr( 'entry-summary' ); ?>>

				<?php df_blog_content(); // function in includes/admin/functions/content-blog.php ?>


			</div><!-- .entry-summary -->
			
			<p class="<?php the_field('read_more'); ?>"><a href="<?php the_permalink(); ?>">Read More</a></p>

			<?php do_action( 'df_image_as_background' ); ?>
			

		<?php endif; // End single post check. ?>

	</div><!-- .wrap -->

	<div class="clear"></div>
	
	<?php if ( is_single( get_the_ID() ) ) : // If viewing a single post. ?>
	<?php else : // If not viewing a single post. ?>
	<hr />
	<?php endif; // End single post check. ?>

</article><!-- .entry -->