<div id="address" class="sidebar large-3 large-offset-4 medium-4 medium-offset-4" role="complementary">

	<?php if ( is_active_sidebar( 'address' ) ) : ?>

		<?php dynamic_sidebar( 'address' ); ?>

	<?php else : ?>

	<!-- This content shows up if there are no widgets defined in the backend. -->
						
	<div class="alert help">
		<p><?php _e( 'Please activate some Widgets.', 'jointswp' );  ?></p>
	</div>

	<?php endif; ?>

</div>