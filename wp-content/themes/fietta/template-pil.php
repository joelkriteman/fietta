<?php
/*
Template Name: PIL News
*/
?>

<?php get_header(); ?>
	
	<div id="content" class="pil">
		
		<div id="inner-content" class="row">
		     
		    <main id="main" class="large-10 large-offset-1 medium-12 columns end" role="main">

				<div class="pil-intro">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
			    	<?php get_template_part( 'parts/loop', 'pil' ); ?>
			    
			    <?php endwhile; endif; ?>
				
				</div>
				
				<?php echo do_shortcode("[cptapagination custom_post_type='pil_news' post_limit='10']"); ?>
				  					
			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>