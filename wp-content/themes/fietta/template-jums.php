<?php
/*
Template Name: JUMS
*/
?>

<?php get_header(); ?>

	<div id="content" class="about">

		<div id="content-wrapper">

			<div id="inner-content" class="row">

			    <header class="large-12 columns article-header">
			    	<h1 class="page-title"><?php the_title(); ?></h1>
			    </header> <!-- end article header -->

			    <main id="main" class="large-6 medium-6 columns" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				    	<?php get_template_part( 'parts/loop', 'page' ); ?>

				    <?php endwhile; endif; ?>

				</main> <!-- end #main -->

				<section id="secondary" class="large-6 medium-6 columns end">

					<?php if( get_field('secondary_column') ): ?>

						<?php the_field('secondary_column'); ?>

					<?php endif; ?>

				</section> <!-- end #secondary -->

			</div> <!-- end #inner-content -->

		</div> <!-- end #content-wrapper -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
