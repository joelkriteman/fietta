<?php
/*
Template Name: Practice
*/
?>

<?php get_header(); ?>

	<div id="content" class="practice">

		<div id="content-wrapper">

			<div id="inner-content">

				<?php if( get_field('introduction') ): ?>

					<div class="row">

						<div class="large-10 large-offset-1 columns">

							<p class="introduction"><?php the_field('introduction'); ?></p>

						</div>

					</div>

				<?php endif; ?>

				<div class="row">

					<main id="main" class="large-6 medium-6 columns">

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				    	<?php get_template_part( 'parts/loop', 'page' ); ?>

				    <?php endwhile; endif; ?>

					</main> <!-- end #main -->

				   <section id="secondary" class="large-6 medium-6 columns">

				    	<?php if( get_field('secondary_column') ): ?>

				    		<?php the_field('secondary_column'); ?>

				    	<?php endif; ?>

				    </section> <!-- end #secondary -->

					</div>

			</div> <!-- end #inner-content -->

		</div> <!-- end #content-wrapper -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
