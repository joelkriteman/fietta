<?php
/*
Template Name: Lawyers
*/
?>

<?php get_header(); ?>

	<div id="content" class="lawyers">

		<div id="inner-content" class="row">

		    <nav class="large-2 large-offset-1 medium-3 columns">

		    	<?php get_sidebar('submenu'); ?>

		    </nav>

		    <main id="main" class="large-9 medium-9 columns end" role="main" data-equalizer>

				<?php

					$lawyerArgs = array(
						'post_type' => 'lawyers',
						'showposts' => -1
					);
					$lawyer = new WP_Query($lawyerArgs);
				?>

				<?php if($lawyer->have_posts()): $lawyerCount = 1; ?>
					<?php while($lawyer->have_posts()): $lawyer->the_post(); ?>

						<div class="large-6 columns lawyer" data-equalizer-watch>

							<div class="row">

								<a href="<?php the_permalink(); ?>">

				    				<div class="large-6 medium-6 columns">


											<?php
												$attachment_id = get_field('photo');
												$size = "lawyersPhoto";
												$image = wp_get_attachment_image_src( $attachment_id, $size );
											?>
											<img class="photo" src="<?php echo $image[0]; ?>" alt="<?php echo $image['alt']; ?>">

				    				</div>

				    				<div class="large-6 medium-6 columns">

										<h2>

											<?php the_title(); ?>

										</h2>

										<h3>

											<?php if( get_field('job_title') ): ?>

												<?php the_field('job_title'); ?>

											<?php endif; ?>

										</h3>

										<hr />

										<ul class="contact-lawyer">

											<li><i class="fa fa-phone" aria-hidden="true"></i>

											<?php if( get_field('telephone') ): ?>

												<?php the_field('telephone'); ?>

											<?php endif; ?>

											</li>

											<li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></li>

										</ul>

									</div>

								</a>

							</div>

						</div>

					<?php $lawyerCount++; endwhile; ?>
				<?php endif; ?> <!-- end lawyers -->

			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
