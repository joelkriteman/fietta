<div id="map" class="sidebar large-6 medium-12 columns end" role="complementary">

	<?php if ( is_active_sidebar( 'map' ) ) : ?>

		<?php dynamic_sidebar( 'map' ); ?>

	<?php else : ?>

	<!-- This content shows up if there are no widgets defined in the backend. -->
						
	<div class="alert help">
		<p><?php _e( 'Please activate some Widgets.', 'jointswp' );  ?></p>
	</div>

	<?php endif; ?>

</div>