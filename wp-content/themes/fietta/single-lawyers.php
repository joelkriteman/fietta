<?php get_header(); ?>
			
<div id="content" class="lawyer">

	<div id="inner-content" class="row">

		<nav class="large-2 large-offset-1 medium-3 columns">
		
			<?php get_sidebar('submenu'); ?>
		
		</nav>
		
		<main id="main" class="large-9 medium-9 columns end" role="main">
		
		    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		
		    	<?php get_template_part( 'parts/loop', 'single-lawyers' ); ?>
		    	
		    <?php endwhile; else : ?>
		
		   		<?php get_template_part( 'parts/content', 'missing' ); ?>

		    <?php endif; ?>

		</main> <!-- end #main -->

	</div> <!-- end #inner-content -->

</div> <!-- end #content -->

<?php get_footer(); ?>