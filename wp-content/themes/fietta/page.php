<?php get_header(); ?>
	
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <header class="large-10 large-offset-1 columns article-header">
		    	<h1 class="page-title"><?php the_title(); ?></h1>
		    </header> <!-- end article header -->
		    
		    <main id="main" class="large-10 large-offset-1 medium-12 columns end" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>							
			    					
			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>