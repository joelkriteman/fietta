<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->

<nav class="top-bar" id="top-bar-menu">
	<div class="top-bar-right float-right hide-for-large">
		<ul class="menu">
			<!--<li><a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></li>-->
			<li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
			<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'main-menu' ); ?></a></li>
		</ul>
	</div>
	<div class="show-for-large">
		<?php joints_top_nav(); ?>	
	</div>
</nav>