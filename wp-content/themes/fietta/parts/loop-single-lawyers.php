<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

	<header class="article-header">
		<div class="row">
			<div class="large-4 medium-4 columns">
				<?php
					$attachment_id = get_field('photo');
					$size = "lawyersPhoto";
					$image = wp_get_attachment_image_src( $attachment_id, $size );
				?>
				<img class="photo" src="<?php echo $image[0]; ?>" alt="<?php echo $image['alt']; ?>">
			</div>
			<div class="large-8 medium-8 columns">

				<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>

				<h2>

					<?php if( get_field('job_title') ): ?>

						<?php the_field('job_title'); ?>

					<?php endif; ?>

				</h2>

				<hr />

				<ul class="contact-lawyer">

					<li><i class="fa fa-phone" aria-hidden="true"></i>

					<?php if( get_field('telephone') ): ?>

						<?php the_field('telephone'); ?>

					<?php endif; ?>

					</li>

					<li><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></li>

				</ul>

			</div>
		</div>
    </header> <!-- end article header -->

    <section class="entry-content" itemprop="articleBody">
		<?php the_post_thumbnail('thumb'); ?>
		<?php the_content(); ?>
	</section> <!-- end article section -->

	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>
	</footer> <!-- end article footer -->

	<?php comments_template(); ?>

</article> <!-- end article -->
