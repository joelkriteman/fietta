<?php
/*
Template Name: Firm News
*/
?>

<?php get_header(); ?>

	<div id="content" class="firmnews">

		<div id="content-wrapper">

			<div id="inner-content" class="row">

			    <main id="main" class="large-12 columns" role="main">

						<?php if( get_field('introduction') ): ?>

							<div class="row">

								<div class="large-10 large-offset-1 columns">

									<p class="introduction"><?php the_field('introduction'); ?></p>

								</div>

							</div>

						<?php endif; ?>

			    	<div class="row">

			    		<div class="large-12 columns">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<?php get_template_part( 'parts/loop', 'page-firmnews' ); ?>

							<?php endwhile; endif; ?>

						</div>

					</div>

				</main> <!-- end #main -->

			</div> <!-- end #inner-content -->

		</div> <!-- end #content-wrapper -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
