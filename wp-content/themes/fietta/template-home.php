<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>

	<div id="content" class="home">

		<div id="home-header" class="row" role="banner">

			<header class="large-12 medium-12 columns text-center">

				<?php
				    echo do_shortcode("[metaslider id=127]");
				?>

			</header>

		</div>

		<div id="content-wrapper">

			<div id="inner-content" class="row">

		    <main id="main" class="large-12 columns" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'home' ); ?>

			    <?php endwhile; endif; ?>

				</main> <!-- end #main -->


			</div> <!-- end #inner-content -->

			<div class="row">

				<div class="large-12 columns">

					<hr>

				</div>

			</div>

			<div id="news-section" class="row">

			    <div id="firm-news" class="large-6 medium-6 columns">

					<?php if( get_field('firm_news') ): ?>

						<?php the_field('firm_news'); ?>

					<?php endif; ?>

					<br />

					<!--<div id="brexit-insights">

						<?php if( get_field('brexit_insight') ): ?>

							<?php the_field('brexit_insight'); ?>

						<?php endif; ?>

					</div>-->

				</div> <!-- end #firm-news -->

			    <?php get_sidebar('pil'); ?>

			</div> <!-- end #news-section -->

			<div id="accreditations" class="row">

				<div class="large-12 column text-center">

					<img class="logo" src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/accreditations.jpg" alt="accreditations" />

				</div>

			</div>

		</div> <!-- end #content-wrapper -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
