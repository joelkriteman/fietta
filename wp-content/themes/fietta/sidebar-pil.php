<div id="pil" class="sidebar large-6 medium-6 columns" role="complementary">

	<?php if ( is_active_sidebar( 'pil' ) ) : ?>

		<?php dynamic_sidebar( 'pil' ); ?>

	<?php else : ?>

	<!-- This content shows up if there are no widgets defined in the backend. -->
						
	<div class="alert help">
		<p><?php _e( 'Please activate some Widgets.', 'jointswp' );  ?></p>
	</div>

	<?php endif; ?>

</div>