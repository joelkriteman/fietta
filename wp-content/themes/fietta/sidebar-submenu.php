<div id="submenu" class="sidebar" role="complementary">

	<?php if ( is_active_sidebar( 'submenu' ) ) : ?>

		<?php dynamic_sidebar( 'submenu' ); ?>

	<?php else : ?>

	<!-- This content shows up if there are no widgets defined in the backend. -->
						
	<div class="alert help">
		<p><?php _e( 'Please activate some Widgets.', 'jointswp' );  ?></p>
	</div>

	<?php endif; ?>

</div>