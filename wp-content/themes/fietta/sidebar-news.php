<div id="news" class="sidebar" role="complementary">

	<?php if ( is_active_sidebar( 'news' ) ) : ?>

		<?php dynamic_sidebar( 'news' ); ?>

	<?php else : ?>

	<!-- This content shows up if there are no widgets defined in the backend. -->
						
	<div class="alert help">
		<p><?php _e( 'Please activate some Widgets.', 'jointswp' );  ?></p>
	</div>

	<?php endif; ?>

</div>