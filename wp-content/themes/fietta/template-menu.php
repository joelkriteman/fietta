<?php
/*
Template Name: Menu
*/
?>

<?php get_header(); ?>
	
	<div id="content" class="menu">
		
		<div id="inner-content" class="row">
	
		    <nav class="large-2 large-offset-1 medium-3 columns">
		    
		    	<?php get_sidebar('submenu'); ?>
		    
		    </nav>
		    
		    <main id="main" class="large-8 medium-9 columns end" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>
			    					
			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>