<?php
/*
Template Name: Internships
*/
?>

<?php get_header(); ?>

	<div id="content" class="internships">

		<div id="internships-header" class="row" role="banner">

			<?php
				$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 2000,1140 ), false, '' );
			?>

			<header class="large-12 medium-12 columns" style="background-image: url(<?php echo $src[0]; ?> ) !important;">

				<div class="row">

					<div class="large-6 large-offset-6 medium-6 medium-offset-6 columns intro">

						<header class="article-header">
							<h1 class="page-title"><?php the_title(); ?></h1>
						</header> <!-- end article header -->

						<?php

						$field_name = "intro";
						$field = get_field_object($field_name);

						echo $field['value'] ;

						?>

					</div>

				</div>

			</header>
			
		</div>

		<!--<div id="inner-content" class="row">

		    <main id="main" class="large-10 large-offset-1 medium-12 columns end" role="main">-->

				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page-internships' ); ?>

			    <?php endwhile; endif; ?>

			<!--</main>

		</div>-->

	</div> <!-- end #content -->

<?php get_footer(); ?>
