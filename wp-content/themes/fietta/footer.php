				<footer class="footer" role="contentinfo">
					<div id="inner-footer" class="row text-center">
						<div class="large-3 medium-3 columns">
							<?php
							    echo do_shortcode("[widget id=black-studio-tinymce-5]");
							?>
						</div>
						<div class="large-2 large-offset-1 medium-3 columns">
							<?php
							    echo do_shortcode("[widget id=black-studio-tinymce-3]");
							?>
						</div>
						<div class="large-6 medium-6 columns">
							<?php
							    echo do_shortcode("[widget id=black-studio-tinymce-4]");
							?>
						</div>
					</div> <!-- end #inner-footer -->
				</footer> <!-- end .footer -->
			</div>  <!-- end .main-content -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
	</body>
</html> <!-- end page -->
